<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only(['create', 'store', 'edit','update']);
    }

    public function index()
    {
        $questions = Question::with('owner')
                                ->latest()
                                ->paginate(10); //eager load

        return view('questions.index', compact([
            'questions'
        ]));
    }

    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');
    }

    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title'=>$request->title,
            'body'=>$request->body
        ]);

        session()->flash('success', "Question has been added successfully!");
        return redirect(route('questions.index'));
    }
    //For hackathon

    public function show($slug){
        $question = Question::with('answers.author')->where('slug', $slug)->firstOrFail();
        return view('questions.show', compact([
                    'question'
                ]));
    }
    // public function show(Question $question)
    // {
    //     $question->increment('views_count');
    //     return view('questions.show', compact([
    //         'question'
    //     ]));
    // }

    public function edit(Question $question)
    {
        // if(Gate:: allows('update-question', $question)){
            if($this->authorize('update', $question)){
        app('debugbar')->disable();
        return view('questions.edit', compact([
            'question'
        ]));
        }
        abort(403);
    }


    public function update(UpdateQuestionRequest $request, Question $question)
    {
        // if(Gate:: allows('update-question', $question)){
        if($this->authorize('update', $question)){
        $question->update([
            'title'=>$request->title,
            'body'=>$request->body
        ]);
        session()->flash('success', "Question has been updated successfully!");
        return redirect(route('questions.index'));
        }
        abort(403);
    }

    public function destroy(Question $question)
    {
        // $question->delete();
        // session()->flash('success', 'Question has been deleted successfully!');
        // return redirect(route('questions.index'));

        // if(auth()->user()->can('delete-question', $question)){  //2nd method
            if($this->authorize('update', $question)){
            $question->delete();
            session()->flash('success', 'Question has been deleted successfully!');
        return redirect(route('questions.index'));

        }
        abort(403);
    }
}
